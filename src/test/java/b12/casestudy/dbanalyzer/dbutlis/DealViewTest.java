/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.dbutlis;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Graduate
 */
public class DealViewTest {
    
    public DealViewTest() {
    }

    /**
     * Test of getDealCost method, of class DealView.
     */
    @Test
    public void testGetDealCost() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealCost();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealCost method, of class DealView.
     */
    @Test
    public void testSetDealCost() {
        
        String itsDealCost = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setDealCost(itsDealCost);
        String expResult = "";
        String result = instance.getDealCost();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealInstrumentName method, of class DealView.
     */
    @Test
    public void testGetDealInstrumentName() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealInstrumentName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealInstrumentName method, of class DealView.
     */
    @Test
    public void testSetDealInstrumentName() {
        
        String itsDealInstrumentName = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setDealInstrumentName(itsDealInstrumentName);
        String expResult = "";
        String result = instance.getDealInstrumentName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealCounterpartyName method, of class DealView.
     */
    @Test
    public void testGetDealCounterpartyName() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealCounterpartyName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealCounterpartyName method, of class DealView.
     */
    @Test
    public void testSetDealCounterpartyName() {
        
        String itsDealCounterpartyName = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setDealCounterpartyName(itsDealCounterpartyName);
        String expResult = "";
        String result = instance.getDealCounterpartyName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealID method, of class DealView.
     */
    @Test
    public void testGetDealID() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealID();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealID method, of class DealView.
     */
    @Test
    public void testSetDealID() {
        
        String dealId = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setDealID(dealId);
        String expResult = "";
        String result = instance.getDealID();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealTime method, of class DealView.
     */
    @Test
    public void testGetDealTime() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealTime method, of class DealView.
     */
    @Test
    public void testSetDealTime() {
        
        String dealTime = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setDealTime(dealTime);
        String expResult = "";
        String result = instance.getDealTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealType method, of class DealView.
     */
    @Test
    public void testGetDealType() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealType method, of class DealView.
     */
    @Test
    public void testSetDealType() {
        
        String dealType = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setDealType(dealType);
        String expResult = "";
        String result = instance.getDealType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealAmount method, of class DealView.
     */
    @Test
    public void testGetDealAmount() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealAmount method, of class DealView.
     */
    @Test
    public void testSetDealAmount() {
        
        String dealAmount = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setDealAmount(dealAmount);
        String expResult = "";
        String result = instance.getDealAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealQuantity method, of class DealView.
     */
    @Test
    public void testGetDealQuantity() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealQuantity();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealQuantity method, of class DealView.
     */
    @Test
    public void testSetDealQuantity() {
        
        String dealQuantity = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setDealQuantity(dealQuantity);
        String expResult = "";
        String result = instance.getDealQuantity();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealInstrumentId method, of class DealView.
     */
    @Test
    public void testGetDealInstrumentId() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealInstrumentId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealInstrumentId method, of class DealView.
     */
    @Test
    public void testSetDealInstrumentId() {
        
        String dealInstrumentId = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setDealInstrumentId(dealInstrumentId);
        String expResult = "";
        String result = instance.getDealInstrumentId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCounterpartyId method, of class DealView.
     */
    @Test
    public void testGetCounterpartyId() {
        
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getCounterpartyId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCounterpartyId method, of class DealView.
     */
    @Test
    public void testSetCounterpartyId() {
        
        String counterpartyId = "";
        DealView instance = new DealView("", "", "", "", "", "", "", "", "", "");
        instance.setCounterpartyId(counterpartyId);
        String expResult = "";
        String result = instance.getCounterpartyId();
        assertEquals(expResult, result);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.dbutlis;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Graduate
 */
public class CounterpartyViewTest {
    
    public CounterpartyViewTest() {
    }

    /**
     * Test of getCounterpartyId method, of class CounterpartyView.
     */
    @Test
    public void testGetCounterpartyId() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getCounterpartyId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCounterpartyId method, of class CounterpartyView.
     */
    @Test
    public void testSetCounterpartyId() {
        
        String itsCounterpartyId = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setCounterpartyId(itsCounterpartyId);
        String expResult = "";
        String result = instance.getCounterpartyId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCounterpartyName method, of class CounterpartyView.
     */
    @Test
    public void testGetCounterpartyName() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getCounterpartyName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCounterpartyName method, of class CounterpartyView.
     */
    @Test
    public void testSetCounterpartyName() {
        
        String itsCounterpartyName = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setCounterpartyName(itsCounterpartyName);
        String expResult = "";
        String result = instance.getCounterpartyName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInstrumentId method, of class CounterpartyView.
     */
    @Test
    public void testGetInstrumentId() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getInstrumentId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInstrumentId method, of class CounterpartyView.
     */
    @Test
    public void testSetInstrumentId() {
        
        String instrumentId = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setInstrumentId(instrumentId);
        String expResult = "";
        String result = instance.getInstrumentId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInstrumentName method, of class CounterpartyView.
     */
    @Test
    public void testGetInstrumentName() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getInstrumentName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInstrumentName method, of class CounterpartyView.
     */
    @Test
    public void testSetInstrumentName() {
        
        String instrumentName = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setInstrumentName(instrumentName);
        String expResult = "";
        String result = instance.getInstrumentName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealQty method, of class CounterpartyView.
     */
    @Test
    public void testGetDealQty() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealQty();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealQty method, of class CounterpartyView.
     */
    @Test
    public void testSetDealQty() {
        
        String dealQty = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setDealQty(dealQty);
        String expResult = "";
        String result = instance.getDealQty();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNetTrades method, of class CounterpartyView.
     */
    @Test
    public void testGetNetTrades() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getNetTrades();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNetTrades method, of class CounterpartyView.
     */
    @Test
    public void testSetNetTrades() {
        
        String netTrades = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setNetTrades(netTrades);
        String expResult = "";
        String result = instance.getNetTrades();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndingPos method, of class CounterpartyView.
     */
    @Test
    public void testGetEndingPos() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getEndingPos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEndingPos method, of class CounterpartyView.
     */
    @Test
    public void testSetEndingPos() {
        
        String endingPos = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setEndingPos(endingPos);
        String expResult = "";
        String result = instance.getEndingPos();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndingSellPrice method, of class CounterpartyView.
     */
    @Test
    public void testGetEndingSellPrice() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getEndingSellPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEndingSellPrice method, of class CounterpartyView.
     */
    @Test
    public void testSetEndingSellPrice() {
        
        String endingSellPrice = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setEndingSellPrice(endingSellPrice);
        String expResult = "";
        String result = instance.getEndingSellPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndingBuyPrice method, of class CounterpartyView.
     */
    @Test
    public void testGetEndingBuyPrice() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getEndingBuyPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEndingBuyPrice method, of class CounterpartyView.
     */
    @Test
    public void testSetEndingBuyPrice() {
        
        String endingBuyPrice = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setEndingBuyPrice(endingBuyPrice);
        String expResult = "";
        String result = instance.getEndingBuyPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRealisedProfit method, of class CounterpartyView.
     */
    @Test
    public void testGetRealisedProfit() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getRealisedProfit();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRealisedProfit method, of class CounterpartyView.
     */
    @Test
    public void testSetRealisedProfit() {
        
        String realisedProfit = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setRealisedProfit(realisedProfit);
        String expResult = "";
        String result = instance.getRealisedProfit();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEffectiveProfit method, of class CounterpartyView.
     */
    @Test
    public void testGetEffectiveProfit() {
        
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getEffectiveProfit();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEffectiveProfit method, of class CounterpartyView.
     */
    @Test
    public void testSetEffectiveProfit() {
        
        String effectiveProfit = "";
        CounterpartyView instance = new CounterpartyView("", "", "", "", "", "", "", "", "", "", "");
        instance.setEffectiveProfit(effectiveProfit);
        String expResult = "";
        String result = instance.getEffectiveProfit();
        assertEquals(expResult, result);
    }
}

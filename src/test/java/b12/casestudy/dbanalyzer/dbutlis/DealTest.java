/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.dbutlis;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;

/**
 *
 * @author Graduate
 */
public class DealTest {
    
    private static Deal deal;
    
    @BeforeClass
    public static void prepareDeal() {
        deal = new Deal("", "", "", "", "", "", "");
    }
    
    public DealTest() {
        Deal emptyDeal = new Deal("");
        assertNotNull(emptyDeal);
        assertEquals("", emptyDeal.getDealID());
    }
    /**
     * Test of getDealID method, of class Deal.
     */
    @Test
    public void testGetDealID() {
        String result = deal.getDealID();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of setDealID method, of class Deal.
     */
    @Test
    public void testSetDealID() {
        deal.setDealID("");
        String expected = "";
        String result = deal.getDealID();
        assertEquals(expected, result);
    }

    /**
     * Test of getDealTime method, of class Deal.
     */
    @Test
    public void testGetDealTime() {
        String result = deal.getDealTime();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of setDealTime method, of class Deal.
     */
    @Test
    public void testSetDealTime() {
        deal.setDealTime("");
        String result = deal.getDealTime();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of getDealType method, of class Deal.
     */
    @Test
    public void testGetDealType() {
        String result = deal.getDealType();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of setDealType method, of class Deal.
     */
    @Test
    public void testSetDealType() {
        deal.setDealType("");
        String result = deal.getDealType();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of getDealAmount method, of class Deal.
     */
    @Test
    public void testGetDealAmount() {
        String result = deal.getDealAmount();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of setDealAmount method, of class Deal.
     */
    @Test
    public void testSetDealAmount() {
        deal.setDealAmount("");
        String result = deal.getDealAmount();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of getDealQuantity method, of class Deal.
     */
    @Test
    public void testGetDealQuantity() {
        String result = deal.getDealQuantity();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of setDealQuantity method, of class Deal.
     */
    @Test
    public void testSetDealQuantity() {
        deal.setDealQuantity("");
        String result = deal.getDealQuantity();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of getDealInstrumentId method, of class Deal.
     */
    @Test
    public void testGetDealInstrumentId() {
        String result = deal.getDealInstrumentId();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of setDealInstrumentId method, of class Deal.
     */
    @Test
    public void testSetDealInstrumentId() {
        deal.setDealInstrumentId("");
        String result = deal.getDealInstrumentId();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of getCounterpartyId method, of class Deal.
     */
    @Test
    public void testGetCounterpartyId() {
        String result = deal.getCounterpartyId();
        String expected = "";
        assertEquals(expected, result);
    }

    /**
     * Test of setCounterpartyId method, of class Deal.
     */
    @Test
    public void testSetCounterpartyId() {
        deal.setCounterpartyId("");
        String result = deal.getCounterpartyId();
        String expected = "";
        assertEquals(expected, result);
    }
}

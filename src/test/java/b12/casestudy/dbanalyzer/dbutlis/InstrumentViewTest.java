/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.dbutlis;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Graduate
 */
public class InstrumentViewTest {
    
    public InstrumentViewTest() {
    }

    /**
     * Test of getInstrumentID method, of class InstrumentView.
     */
    @Test
    public void testGetInstrumentID() {
        
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getInstrumentID();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInstrumentID method, of class InstrumentView.
     */
    @Test
    public void testSetInstrumentID() {
        
        String itsInstrumentID = "";
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        instance.setInstrumentID(itsInstrumentID);
        String expResult = "";
        String result = instance.getInstrumentID();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInstrumentName method, of class InstrumentView.
     */
    @Test
    public void testGetInstrumentName() {
        
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getInstrumentName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setInstrumentName method, of class InstrumentView.
     */
    @Test
    public void testSetInstrumentName() {
        
        String itsInstrumentName = "";
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        instance.setInstrumentName(itsInstrumentName);
        String expResult = "";
        String result = instance.getInstrumentName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDealType method, of class InstrumentView.
     */
    @Test
    public void testGetDealType() {
        
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getDealType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDealType method, of class InstrumentView.
     */
    @Test
    public void testSetDealType() {
        
        String itsDealType = "";
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        instance.setDealType(itsDealType);
        String expResult = "";
        String result = instance.getDealType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMinAmount method, of class InstrumentView.
     */
    @Test
    public void testGetMinAmount() {
        
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getMinAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMinAmount method, of class InstrumentView.
     */
    @Test
    public void testSetMinAmount() {
        
        String itsMinAmount = "";
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        instance.setMinAmount(itsMinAmount);
        String expResult = "";
        String result = instance.getMinAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMaxAmount method, of class InstrumentView.
     */
    @Test
    public void testGetMaxAmount() {
        
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getMaxAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMaxAmount method, of class InstrumentView.
     */
    @Test
    public void testSetMaxAmount() {
        
        String itsMaxAmount = "";
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        instance.setMaxAmount(itsMaxAmount);
        String expResult = "";
        String result = instance.getMaxAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAvgAmount method, of class InstrumentView.
     */
    @Test
    public void testGetAvgAmount() {
        
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getAvgAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAvgAmount method, of class InstrumentView.
     */
    @Test
    public void testSetAvgAmount() {
        
        String itsAvgAmount = "";
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        instance.setAvgAmount(itsAvgAmount);
        String expResult = "";
        String result = instance.getAvgAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEndingPrice method, of class InstrumentView.
     */
    @Test
    public void testGetEndingPrice() {
        
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        String expResult = "";
        String result = instance.getEndingPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEndingPrice method, of class InstrumentView.
     */
    @Test
    public void testSetEndingPrice() {
        
        String itsEndingPrice = "";
        InstrumentView instance = new InstrumentView("", "", "", "", "", "", "");
        instance.setEndingPrice(itsEndingPrice);
        String expResult = "";
        String result = instance.getEndingPrice();
        assertEquals(expResult, result);
    }
    
}

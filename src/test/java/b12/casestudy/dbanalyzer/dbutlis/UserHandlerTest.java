/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.dbutlis;

import b12.casestudy.dbanalyzer.core.ApplicationScopeHelper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Graduate
 */
public class UserHandlerTest {

    public UserHandlerTest() {
    }

    /**
     * Test of loadFromDB method, supplying empty user credentials and expecting
     * a null return
     */  
    @Test
    @Ignore
    public void test_loadFromDB_empty_user_returns_null() {

        Connection theConnection = null;
        try {
            theConnection = DriverManager.getConnection("jdbc:mysql://localhost:3307/db_grad", "root", "ppp");
        } catch (SQLException ex) {
            Logger.getLogger(UserHandlerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        String userid = "";
        String pwd = "";
        UserHandler instance = UserHandler.getLoader();
        User expResult = null;
        User result = instance.loadFromDB(theConnection, userid, pwd);
        assertEquals(expResult, result);
    }

    /**
     * Test of loadFromDB method, supplying valid user credentials and expecting
     * a user id return
     */  
    @Ignore
    @Test
    public void test_loadFromDB_valid_user_returns_id() {
        ApplicationScopeHelper ash = new ApplicationScopeHelper();
        assertEquals(true, ash.bootstrapDBConnection());
//        Connection theConnection = null;
//        try {
//            theConnection = DriverManager.getConnection("jdbc:mysql://localhost:3307/db_grad", "root", "ppp");
//        } catch (SQLException ex) {
//            Logger.getLogger(UserHandlerTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
        String userid = "alison";
        String pwd = "gradprog2016@07";
        UserHandler instance = UserHandler.getLoader();
        String expResult = "alison";
        String result = "";
        try {
            result = instance.loadFromDB(DBConnector.getConnector().getConnection(), userid, pwd).getUserID();
        } catch (IOException ex) {
            Logger.getLogger(UserHandlerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(expResult, result);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.core;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Ignore;

/**
 *
 * @author Graduate
 */
public class DealControllerTest {
    private static boolean connectionStatus = false;
    public DealControllerTest() {
    }
    @BeforeClass
    public static void setup_database_connection()
    {
        connectionStatus = (new ApplicationScopeHelper()).bootstrapDBConnection();
//
//        if (!connectionStatus) {
//            fail();
//        }
    }


    /**
     * Test of getTable method, of class DealController.
     */
    @Test
    public void testGetTable() {
        if (connectionStatus == true) {
            String column = null;
            String filter = null;
            String sort = null;
            String sortOrder = null;
            String limit = null;
            String offset = null;
            DealController instance = new DealController();
            String expResult = "";
            String result = instance.getTable(column, filter, sort, sortOrder, limit, offset);
            assertNotNull(result);
        } else {
            assertFalse(connectionStatus);
        }

    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.User;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Ignore;


/**
 *
 * @author Graduate
 */
public class ApplicationScopeHelperTest {
    
    private static boolean connectionStatus = false;
    
    public ApplicationScopeHelperTest() {
    }

    @BeforeClass
    public static void setup_database_connection()
    {
        connectionStatus = (new ApplicationScopeHelper()).bootstrapDBConnection();

//        if (!connectionStatus) {
//            fail();
//        }
    }

    /**
     * Test of userLogin method, supplying empty user credentials and expecting
     * a null return
     */
    @Test
    public void test_userLogin_empty_user_returns_null() {
        if (connectionStatus == true) {
            String userId = "";
            String userPwd = "";
            ApplicationScopeHelper instance = new ApplicationScopeHelper();
            User expResult = null;
            User result = instance.userLogin(userId, userPwd);
            assertEquals(expResult, result);
        } else {
            assertFalse(connectionStatus);
        }
    }
    
     /**
     * Test of userLogin method, supplying valid user credentials and expecting
     * a user id return
     */
    @Test
    public void test_userLogin_valid_user_returns_id() {
        if (connectionStatus == true) {
            String userId = "alison";
            String userPwd = "gradprog2016@07";
            ApplicationScopeHelper instance = new ApplicationScopeHelper();
            String expResult = "alison";
            String result = instance.userLogin(userId, userPwd).getUserID();
            assertEquals(expResult, result);
        } else {
            assertFalse(connectionStatus);
        }
    }

     /**
     * Test of bootstrapDBConnection method, successfully initialising a new database
     * connection and expecting true as a return
     */
    @Test
    public void test_bootstrapDBConnection_successfull_connection_returns_true() {
        if (connectionStatus == true) {
            ApplicationScopeHelper instance = new ApplicationScopeHelper();
            boolean expResult = true;
            boolean result = instance.bootstrapDBConnection();
            assertEquals(expResult, result);
        } else {
            assertFalse(connectionStatus);
        }
    }
    
}

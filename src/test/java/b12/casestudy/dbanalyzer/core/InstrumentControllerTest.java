/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.UserHandlerTest;
import com.google.common.base.Predicates;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import static org.hamcrest.Matchers.matchesPattern;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Ignore;

/**
 *
 * @author Graduate
 */
public class InstrumentControllerTest {
    private static boolean connectionStatus = false;
    @BeforeClass
    public static void setup_database_connection()
    {
        connectionStatus = (new ApplicationScopeHelper()).bootstrapDBConnection();

//        if (!connectionStatus) {
//            fail();
//        }
    }


    /**
     * Test of getTable method, of class InstrumentController.
     */
    @Test
    public void testGetTable() {
        if (connectionStatus) {
            String column = null;
            String filter = null;
            String sort = null;
            String sortOrder = null;
            String limit = null;
            String offset = null;
            InstrumentController instance = new InstrumentController();
            String result = instance.getTable(column, filter, sort, sortOrder, limit, offset);
            assertNotNull(result);
        } else {
            assertFalse(connectionStatus);
        }
//        String result = instance.getTable(column, filter, sort, sortOrder, limit, offset);
        
        
//        String regex = "^\\[\\{\\\"size\\\":\\\".+\\\"\\},[\\r\\n\\s\\t]+(\\{((\\\"instrumentName\\\":\\\".+\\\".*)|(\\\"endingPrice\\\":\\\".+\\\".*)|(\\\"instrumentID\\\":\\\".+\\\"))+\\},[\\r\\n\\s\\t]+)+\\{\\\"instrumentName\\\":\\\".+\\\",\\\"endingPrice\\\":\\\".+\\\",\\\"instrumentID\\\":\\\".+\\\"\\}\\]$";
//        Pattern MY_PATTERN = Pattern.compile(regex);
//        java.util.regex.Matcher m = MY_PATTERN.matcher(result);
//        while (m.find()) {
//            String s = m.group(1);
//            int a = 1;
//            // s now contains "BAR"
//        }
        
//        Matcher<String> aaa = Matchers.matchesPattern("^\\[\\{\\\"size\\\":\\\".+\\\"\\},[\\r\\n\\t]+(\\{((\\\"instrumentName\\\":\\\".+\\\".*)|(\\\"endingPrice\\\":\\\".+\\\".*)|(\\\"instrumentID\\\":\\\".+\\\"))+\\},[\\r\\n\\s\\t]+)+\\{\\\"instrumentName\\\":\\\".+\\\",\\\"endingPrice\\\":\\\".+\\\",\\\"instrumentID\\\":\\\".+\\\"\\}\\]$");
//        aaa.matches(result);
//        assertThat(result, Matchers.matchesPattern("^\\[\\{\\\"size\\\":\\\".+\\\"\\},[\\r\\n\\s\\t]+(\\{((\\\"instrumentName\\\":\\\".+\\\".*)|(\\\"endingPrice\\\":\\\".+\\\".*)|(\\\"instrumentID\\\":\\\".+\\\"))+\\},[\\r\\n\\s\\t]+)+\\{\\\"instrumentName\\\":\\\".+\\\",\\\"endingPrice\\\":\\\".+\\\",\\\"instrumentID\\\":\\\".+\\\"\\}\\]$"));
                //+ "({\"instrumentName\":\".+\",\"endingPrice\":\".+\"},\"instrumentID\":\".+\".*\n)*.*"));


    }
    
}

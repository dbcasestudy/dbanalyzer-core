/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.core;

import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Graduate
 */
public class UserControllerTest {
    private static boolean connectionStatus = false;

    public UserControllerTest() {
    }

    /**
     * Test of verifyLoginDetails method, supplying empty user credentials and
     * expecting null as return
     */
    @Test
    public void test_verifyLoginDetails_empty_login_credentials_returns_null() {
        ApplicationScopeHelper ash = new ApplicationScopeHelper();
        connectionStatus = ash.bootstrapDBConnection();
        if (connectionStatus == true) {
            String userId = "";
            String userPwd = "";
            UserController instance = new UserController();
            String expResult = null;
            String result = instance.verifyLoginDetails(userId, userPwd);
            assertEquals(expResult, result);
        } else {
            assertFalse(connectionStatus);
        }
    }

    /**
     * Test of verifyLoginDetails method, supplying valid user credentials and
     * expecting a return of database record containing the credentials
     */
    @Test
    public void test_verifyLoginDetails_actual_user_returns_db_record() {
        ApplicationScopeHelper ash = new ApplicationScopeHelper();
        connectionStatus = ash.bootstrapDBConnection();
        if (connectionStatus == true) {
            String userId = "alison";
            String userPwd = "gradprog2016@07";
            UserController instance = new UserController();
            String expResult = userId;
            String result = instance.verifyLoginDetails(userId, userPwd);
            assertEquals(result, expResult);
        } else {
            assertFalse(connectionStatus);
        }
    }

}

package b12.casestudy.dbanalyzer.dbutlis;

/**
 *
 * @author B12
 */
public class Counterparty {

    private String itsCounterpartyId;
    private String itsCounterpartyName;
    private String itsCounterpartyStatus;
    private String itsCounterpartyDateRegistered;
    private String realisedProfit;
    private String effectiveProfit;

    public Counterparty(String counterpartyId) {
        itsCounterpartyId = counterpartyId;
    }

    public Counterparty(String counterpartyId, String counterpartyName,
            String counterpartyStatus, String counterpartyDateRegistered,
            String realisedProfit, String effectiveProfit) {
        itsCounterpartyId = counterpartyId;
        itsCounterpartyName = counterpartyName;
        itsCounterpartyStatus = counterpartyStatus;
        itsCounterpartyDateRegistered = counterpartyDateRegistered;
        this.realisedProfit = realisedProfit;
        this.effectiveProfit = effectiveProfit;
    }

    public String getCounterpartyId() {
        return itsCounterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.itsCounterpartyId = counterpartyId;
    }

    public String getCounterpartyName() {
        return itsCounterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.itsCounterpartyId = counterpartyName;
    }

    public String getCounterpartyStatus() {
        return itsCounterpartyStatus;
    }

    public void setCounterpartyStatus(String counterpartyStatus) {
        this.itsCounterpartyStatus = counterpartyStatus;
    }

    public String getCounterpartyDateRegistered() {
        return itsCounterpartyDateRegistered;
    }

    public void setCounterpartyDateRegistered(String counterpartyDateRegistered) {
        this.itsCounterpartyDateRegistered = counterpartyDateRegistered;
    }

    public String getRealisedProfit() {
        return realisedProfit;
    }

    public void setRealisedProfit(String realisedProfit) {
        this.realisedProfit = realisedProfit;
    }

    public String getEffectiveProfit() {
        return effectiveProfit;
    }

    public void setEffectiveProfit(String effectiveProfit) {
        this.effectiveProfit = effectiveProfit;
    }

}

package b12.casestudy.dbanalyzer.dbutlis;

/**
 *
 * @author B12
 */
public class CounterpartyView {

    private String counterpartyId;
    private String counterpartyName;
    private String instrumentId;
    private String instrumentName;
    private String dealQty;
    private String netTrades;
    private String endingPos;
    private String endingSellPrice;
    private String endingBuyPrice;
    private String realisedProfit;
    private String effectiveProfit;

    public String getCounterpartyId() {
        return counterpartyId;
    }

    public void setCounterpartyId(String itsCounterpartyId) {
        this.counterpartyId = itsCounterpartyId;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String itsCounterpartyName) {
        this.counterpartyName = itsCounterpartyName;
    }

    public String getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(String instrumentId) {
        this.instrumentId = instrumentId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getDealQty() {
        return dealQty;
    }

    public void setDealQty(String dealQty) {
        this.dealQty = dealQty;
    }

    public String getNetTrades() {
        return netTrades;
    }

    public void setNetTrades(String netTrades) {
        this.netTrades = netTrades;
    }

    public String getEndingPos() {
        return endingPos;
    }

    public void setEndingPos(String endingPos) {
        this.endingPos = endingPos;
    }

    public String getEndingSellPrice() {
        return endingSellPrice;
    }

    public void setEndingSellPrice(String endingSellPrice) {
        this.endingSellPrice = endingSellPrice;
    }

    public String getEndingBuyPrice() {
        return endingBuyPrice;
    }

    public void setEndingBuyPrice(String endingBuyPrice) {
        this.endingBuyPrice = endingBuyPrice;
    }

    public String getRealisedProfit() {
        return realisedProfit;
    }

    public void setRealisedProfit(String realisedProfit) {
        this.realisedProfit = realisedProfit;
    }

    public String getEffectiveProfit() {
        return effectiveProfit;
    }

    public void setEffectiveProfit(String effectiveProfit) {
        this.effectiveProfit = effectiveProfit;
    }

    public CounterpartyView(String counterpartyId) {
        this.counterpartyId = counterpartyId;
    }

    public CounterpartyView(String counterpartyId, String counterpartyName,
            String instrumentId, String instrumentName, String dealQty,
            String netTrades, String endingPos, String endingSellPrice,
            String endingBuyPrice, String realisedProfit, String effectiveProfit) {
        this.counterpartyId = counterpartyId;
        this.counterpartyName = counterpartyName;
        this.instrumentId = instrumentId;
        this.instrumentName = instrumentName;
        this.dealQty = dealQty;
        this.netTrades = netTrades;
        this.endingPos = endingPos;
        this.endingSellPrice = endingSellPrice;
        this.endingBuyPrice = endingBuyPrice;
        this.realisedProfit = realisedProfit;
        this.effectiveProfit = effectiveProfit;

    }
}

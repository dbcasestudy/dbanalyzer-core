package b12.casestudy.dbanalyzer.dbutlis;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class UserHandler {

    static private UserHandler itsSelf = null;

    private UserHandler() {
    }

    static public UserHandler getLoader() {
        if (itsSelf == null) {
            itsSelf = new UserHandler();
        }
        return itsSelf;
    }

    public User loadFromDB(Connection theConnection, String userid, String pwd) {
        User result = null;
        String sbQuery = "select user_id, user_pwd from creds where user_id=? and user_pwd=?";
        try (PreparedStatement stmt = theConnection.prepareStatement(sbQuery)) {
            String encryptedPwd = Hashing.sha256().hashString(pwd, Charsets.UTF_8).toString();
            stmt.setString(1, userid);
            stmt.setString(2, encryptedPwd);
            ResultSet rs = stmt.executeQuery();

            UserIterator iter = new UserIterator(rs);

            if (rs.next()) {
                result = iter.buildUser();
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }

        return result;
    }

    public String toJSON(User theUser) {
        String result = "<Some value from the server>";
        try {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(theUser);
        } catch (IOException ex) {
            Logger.getLogger(UserHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

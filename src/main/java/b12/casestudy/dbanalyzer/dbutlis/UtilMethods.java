/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b12.casestudy.dbanalyzer.dbutlis;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Graduate
 */
public class UtilMethods {
    public String getFilterExpression(String column, String filter) {
        String finalColumnToFilterValueExpression = "";
        String[] columnParsedByComma = column.split(",");
        String[] filterParsedByComma = filter.split(",");

        for (int currentIndex = 0; currentIndex
                < columnParsedByComma.length; ++currentIndex) {
            if (currentIndex != columnParsedByComma.length - 1) {
                finalColumnToFilterValueExpression
                        += columnParsedByComma[currentIndex] + " like "
                        + "\"%" + filterParsedByComma[currentIndex]
                        + "%\"" + " and ";
            } else {
                finalColumnToFilterValueExpression
                        += columnParsedByComma[currentIndex] + " like "
                        + "\"%" + filterParsedByComma[currentIndex] + "%\"";
            }
        }
        return finalColumnToFilterValueExpression;
    }
    
    public String createQueryString(String baseQuery, String column, 
            String filter, String sort,String sortOrder, String limit, 
            String offset) {
        String sbQuery = "";

        String finalColumnToFilterValueExpression = "";
        if (filter != null && column != null) {
            finalColumnToFilterValueExpression = getFilterExpression(column, filter);
        }

        if (sort != null && sortOrder != null && column == null && filter == null) {
            sbQuery = baseQuery + " order by " + sort + " " + sortOrder;
        } else if (column != null && filter != null && sort == null) {
            sbQuery = baseQuery + " where " + finalColumnToFilterValueExpression;
        } else if (column != null && filter != null && sort != null
                && sortOrder != null) {
            sbQuery = baseQuery + " where "
                    + finalColumnToFilterValueExpression + " order by " + sort
                    + " " + sortOrder;

        } else {
            sbQuery = baseQuery;
        }
        if (limit != null) {
            sbQuery += " limit " + limit;
        }
        if (offset != null) {
            sbQuery += " offset " + offset;
        }
        return sbQuery;
    }
    
    public ResultSet getQueryResult(Connection theConnection, String sbQuery) {
        ResultSet rs = null;
        try {
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            rs = stmt.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(UtilMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }

    public void execute(Connection theConnection, String sbQuery) {
        try {
            for (String createStatement : sbQuery.split(";")) {
                Statement stmt = theConnection.createStatement();
                stmt.execute(createStatement);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UtilMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

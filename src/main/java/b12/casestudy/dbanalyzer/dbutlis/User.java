package b12.casestudy.dbanalyzer.dbutlis;

/**
 *
 * @author B12
 */
public class User {

    private String itsUserID;
    private String itsUserPwd;

    public User(String userid, String pwd) {
        itsUserID = userid;
        itsUserPwd = pwd;
    }

    public String getUserID() {
        return itsUserID;
    }

    public void setUserID(String itsUserID) {
        this.itsUserID = itsUserID;
    }

    public String getUserPwd() {
        return itsUserPwd;
    }

    public void setUserPwd(String itsUserPwd) {
        this.itsUserPwd = itsUserPwd;
    }
}

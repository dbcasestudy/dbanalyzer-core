package b12.casestudy.dbanalyzer.dbutlis;

/**
 *
 * @author B12
 */
public class Deal {

//    public Deal() {
//        itsDealId = "";
//        itsDealCounterpartyId = "";
//        itsDealInstrumentId = "";
//        itsDealQuantity = "";
//    }
    public Deal(String dealId) {
        itsDealId = dealId;
    }

    public Deal(String dealId, String counterpartyId, String instrumentId,
            String quantity) {
        itsDealId = dealId;
        itsDealCounterpartyId = counterpartyId;
        itsDealInstrumentId = instrumentId;
        itsDealQuantity = quantity;
    }

    public Deal(String dealId, String time, String type,
            String amount, String quantity, String counterpartyId, String instrumentId) {
        itsDealId = dealId;
        itsDealCounterpartyId = counterpartyId;
        itsDealInstrumentId = instrumentId;
        itsDealQuantity = quantity;
        itsDealAmount = amount;
        itsDealTime = time;
        itsDealType = type;
    }

    private String itsDealId;

    public String getDealID() {
        return itsDealId;
    }

    public void setDealID(String dealId) {
        this.itsDealId = dealId;
    }

    private String itsDealTime;

    public String getDealTime() {
        return itsDealTime;
    }

    public void setDealTime(String dealTime) {
        this.itsDealTime = dealTime;
    }

    private String itsDealType;

    public String getDealType() {
        return itsDealType;
    }

    public void setDealType(String dealType) {
        this.itsDealType = dealType;
    }

    private String itsDealAmount;

    public String getDealAmount() {
        return itsDealAmount;
    }

    public void setDealAmount(String dealAmount) {
        this.itsDealAmount = dealAmount;
    }

    private String itsDealQuantity;

    public String getDealQuantity() {
        return itsDealQuantity;
    }

    public void setDealQuantity(String dealQuantity) {
        this.itsDealQuantity = dealQuantity;
    }

    private String itsDealInstrumentId;

    public String getDealInstrumentId() {
        return itsDealInstrumentId;
    }

    public void setDealInstrumentId(String dealInstrumentId) {
        this.itsDealInstrumentId = dealInstrumentId;
    }

    private String itsDealCounterpartyId;

    public String getCounterpartyId() {
        return itsDealCounterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.itsDealCounterpartyId = counterpartyId;
    }

//    @Override
//    public String toString() {
////        return getCounterpartyId()
//    }
}

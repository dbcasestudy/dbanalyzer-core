package b12.casestudy.dbanalyzer.dbutlis;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class ViewDealHandler {

    static private ViewDealHandler itsSelf = null;
    private ViewDealHandler() {
    }

    static public ViewDealHandler getLoader() {
        if (itsSelf == null) {
            itsSelf = new ViewDealHandler();
        }
        return itsSelf;
    }
    
    public DealView[] loadFromDB(Connection theConnection, String baseQuery, String column, 
            String filter, String sort, String sortOrder, String limit, String offset) {
        UtilMethods utilMethods = new UtilMethods();
        DealView[] result = null;
        try {
            ArrayList<DealView> tmpResult = new ArrayList<>();
            String sbQuery = utilMethods.createQueryString(baseQuery, column, filter, sort, 
                    sortOrder, limit, offset);

            ResultSet rs = utilMethods.getQueryResult(theConnection, sbQuery);
            
            ViewDealIterator iter = new ViewDealIterator(rs);
            
            while (iter.next()) {
                tmpResult.add(iter.build());
            }
            result = new DealView[tmpResult.size()];
            result = tmpResult.toArray(result);
        } catch (SQLException ex) {
            Logger.getLogger(ViewDealHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public String toJSON(DealView theDealView) {
        String result = "<Some value from the server>";
        try {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(theDealView);
        } catch (IOException ex) {
            Logger.getLogger(ViewDealHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

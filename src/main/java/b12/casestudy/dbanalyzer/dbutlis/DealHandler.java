package b12.casestudy.dbanalyzer.dbutlis;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class DealHandler {

    static private DealHandler itsSelf = null;
    private DealHandler() {
    }

    static public DealHandler getLoader() {
        if (itsSelf == null) {
            itsSelf = new DealHandler();
        }
        return itsSelf;
    }
    
    public Deal[] loadFromDB(Connection theConnection, String baseQuery, String column, 
            String filter, String sort, String sortOrder, String limit, String offset) {
        Deal[] result = null;
        try {
            ArrayList<Deal> tmpResult = new ArrayList<>();
            UtilMethods utilMethods = new UtilMethods();

            String sbQuery = utilMethods.createQueryString(baseQuery, column, filter, sort, 
                    sortOrder, limit, offset);

            ResultSet rs = utilMethods.getQueryResult(theConnection, sbQuery);
            
            DealIterator iter = new DealIterator(rs);
            
            while (iter.next()) {
                tmpResult.add(iter.build());
            }
            result = new Deal[tmpResult.size()];
            result = tmpResult.toArray(result);
        } catch (SQLException ex) {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public String toJSON(Deal theDeal) {
        String result = "<Some value from the server>";
        try {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(theDeal);
        } catch (IOException ex) {
            Logger.getLogger(DealHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

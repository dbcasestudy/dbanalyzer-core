package b12.casestudy.dbanalyzer.dbutlis;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class CounterpartyHandler {

    static private CounterpartyHandler itsSelf = null;
    private CounterpartyHandler() {
    }

    static public CounterpartyHandler getLoader() {
        if (itsSelf == null) {
            itsSelf = new CounterpartyHandler();
        }
        return itsSelf;
    }

    public Counterparty[] loadFromDB(Connection theConnection, String baseQuery, 
            String column, String filter, String sort, String sortOrder, String limit, 
            String offset) {
        Counterparty[] result = null;
        try {
            ArrayList<Counterparty> tmpResult = new ArrayList<>();

            UtilMethods utilMethods = new UtilMethods();
            
            String sbQuery = utilMethods.createQueryString(baseQuery, column, filter, sort, 
                    sortOrder, limit, offset);
            
            ResultSet rs = utilMethods.getQueryResult(theConnection, sbQuery);
            
            CounterpartyIterator iter = new CounterpartyIterator(rs);
            while (iter.next()) {
                tmpResult.add(iter.build());
            }
            result = new Counterparty[tmpResult.size()];
            result = tmpResult.toArray(result);
        }
        catch (SQLException ex) {
                Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String toJSON(Counterparty theCounterparty) {
        String result = "<Some value from the server>";
        try {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(theCounterparty);
        } catch (IOException ex) {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

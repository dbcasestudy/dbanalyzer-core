package b12.casestudy.dbanalyzer.dbutlis;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author B12
 */
public class ViewDealIterator {

    ResultSet rowIterator;

    ViewDealIterator(ResultSet rs) {
        rowIterator = rs;
    }

    public boolean first() throws SQLException {
        return rowIterator.first();
    }

    public boolean last() throws SQLException {
        return rowIterator.last();
    }

    public boolean next() throws SQLException {
        return rowIterator.next();
    }

    public boolean prior() throws SQLException {
        return rowIterator.previous();
    }

    public String getDealId() throws SQLException {
        return rowIterator.getString("deal_id");
    }

    public String getDealTime() throws SQLException {
        return rowIterator.getString("deal_time");
    }

    public String getDealType() throws SQLException {
        return rowIterator.getString("deal_type");
    }

    public String getDealAmount() throws SQLException {
        return rowIterator.getString("deal_amount");
    }

    public String getDealQuantity() throws SQLException {
        return rowIterator.getString("deal_quantity");
    }

    public String getDealInstrumentId() throws SQLException {
        return rowIterator.getString("deal_instrument_id");
    }

    public String getCounterpartyId() throws SQLException {
        return rowIterator.getString("deal_counterparty_id");
    }

    public String getDealInstrumentName() throws SQLException {
        return rowIterator.getString("deal_instrument_name");
    }

    public String getDealCounterpartyName() throws SQLException {
        return rowIterator.getString("deal_counterparty_name");
    }

    public String getDealCost() throws SQLException {
        return rowIterator.getString("deal_cost");
    }

    DealView build() throws SQLException {
        DealView result = new DealView(getDealId(), getDealTime(),
                getDealType(), getDealAmount(), getDealQuantity(),
                getCounterpartyId(), getDealInstrumentId(), getDealInstrumentName(),
                getDealCounterpartyName(), getDealCost());
        return result;
    }
} // select deal_id, deal_date, deal_time, deal_type, deal_amount, deal_quantity, deal_counterparty_id from deal

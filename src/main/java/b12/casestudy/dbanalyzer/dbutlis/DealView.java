package b12.casestudy.dbanalyzer.dbutlis;

/**
 *
 * @author B12
 */
public class DealView {

    private String itsDealInstrumentName;
    private String itsDealCounterpartyName;
    private String itsDealCost;

    public DealView(String dealId) {
        itsDealId = dealId;
    }

    public DealView(String dealId, String time, String type,
            String amount, String quantity, String counterpartyId,
            String instrumentId, String dealInstrumentName,
            String dealCounterpartyName, String dealCost) {
        itsDealId = dealId;
        itsDealCounterpartyId = counterpartyId;
        itsDealInstrumentId = instrumentId;
        itsDealQuantity = quantity;
        itsDealAmount = amount;
        itsDealTime = time;
        itsDealType = type;
        itsDealInstrumentName = dealInstrumentName;
        itsDealCounterpartyName = dealCounterpartyName;
        itsDealCost = dealCost;
    }

    public String getDealCost() {
        return itsDealCost;
    }

    public void setDealCost(String itsDealCost) {
        this.itsDealCost = itsDealCost;
    }

    public String getDealInstrumentName() {
        return itsDealInstrumentName;
    }

    public void setDealInstrumentName(String itsDealInstrumentName) {
        this.itsDealInstrumentName = itsDealInstrumentName;
    }

    public String getDealCounterpartyName() {
        return itsDealCounterpartyName;
    }

    public void setDealCounterpartyName(String itsDealCounterpartyName) {
        this.itsDealCounterpartyName = itsDealCounterpartyName;
    }

    private String itsDealId;

    public String getDealID() {
        return itsDealId;
    }

    public void setDealID(String dealId) {
        this.itsDealId = dealId;
    }

    private String itsDealTime;

    public String getDealTime() {
        return itsDealTime;
    }

    public void setDealTime(String dealTime) {
        this.itsDealTime = dealTime;
    }

    private String itsDealType;

    public String getDealType() {
        return itsDealType;
    }

    public void setDealType(String dealType) {
        this.itsDealType = dealType;
    }

    private String itsDealAmount;

    public String getDealAmount() {
        return itsDealAmount;
    }

    public void setDealAmount(String dealAmount) {
        this.itsDealAmount = dealAmount;
    }

    private String itsDealQuantity;

    public String getDealQuantity() {
        return itsDealQuantity;
    }

    public void setDealQuantity(String dealQuantity) {
        this.itsDealQuantity = dealQuantity;
    }

    private String itsDealInstrumentId;

    public String getDealInstrumentId() {
        return itsDealInstrumentId;
    }

    public void setDealInstrumentId(String dealInstrumentId) {
        this.itsDealInstrumentId = dealInstrumentId;
    }

    private String itsDealCounterpartyId;

    public String getCounterpartyId() {
        return itsDealCounterpartyId;
    }

    public void setCounterpartyId(String counterpartyId) {
        this.itsDealCounterpartyId = counterpartyId;
    }
}

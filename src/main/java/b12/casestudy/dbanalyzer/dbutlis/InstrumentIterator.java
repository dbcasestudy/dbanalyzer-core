package b12.casestudy.dbanalyzer.dbutlis;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author B12
 */
public class InstrumentIterator {

    ResultSet rowIterator = null;

    InstrumentIterator(ResultSet rs) {
        if (rs != null) {
            rowIterator = rs;
        }
    }

    public boolean first() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.first();
        }
        return false;
    }

    public boolean last() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.last();
        }
        return false;
    }

    public boolean next() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.next();
        }
        return false;
    }

    public boolean prior() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.previous();
        }
        return false;
    }
    
    public String getInstrumentID() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("instrument_id");
        }
        return null;
    }

    public String getInstrumentName() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("instrument_name");
        }
        return null;
    }
    
    public String getEndingPrice() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("ending_price");
        }
        return null;
    }
    
    Instrument buildInstrument() throws SQLException {
        Instrument result = new Instrument(getInstrumentID(), 
                getInstrumentName(), getEndingPrice());

        return result;
    }
}

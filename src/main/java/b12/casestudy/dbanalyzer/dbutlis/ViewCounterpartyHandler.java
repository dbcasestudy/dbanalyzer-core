package b12.casestudy.dbanalyzer.dbutlis;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class ViewCounterpartyHandler {

    static private ViewCounterpartyHandler itsSelf = null;

    private ViewCounterpartyHandler() {
    }

    static public ViewCounterpartyHandler getLoader() {
        if (itsSelf == null) {
            itsSelf = new ViewCounterpartyHandler();
        }
        return itsSelf;
    }
    
    public CounterpartyView[] loadFromDB(Connection theConnection, String baseQuery, String column,
            String filter, String sort, String sortOrder, String limit, String offset) {
        UtilMethods utilMethods = new UtilMethods();
        CounterpartyView[] result = null;
        try {
            ArrayList<CounterpartyView> tmpResult = new ArrayList<>();
            String sbQuery = utilMethods.createQueryString(baseQuery, column, filter, sort,
                    sortOrder, limit, offset);

            ResultSet rs = utilMethods.getQueryResult(theConnection, sbQuery);

            ViewCounterpartyIterator iter = new ViewCounterpartyIterator(rs);

            while (iter.next()) {
                tmpResult.add(iter.build());
            }
            result = new CounterpartyView[tmpResult.size()];
            result = tmpResult.toArray(result);
        } catch (SQLException ex) {
            Logger.getLogger(ViewCounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public String toJSON(CounterpartyView theCounterpartyView) {
        String result = "<Some value from the server>";
        try {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(theCounterpartyView);
        } catch (IOException ex) {
            Logger.getLogger(ViewCounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

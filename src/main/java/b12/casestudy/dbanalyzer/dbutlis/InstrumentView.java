package b12.casestudy.dbanalyzer.dbutlis;

/**
 *
 * @author B12
 */
public class InstrumentView {

    private String itsInstrumentID;
    private String itsInstrumentName;
    private String itsDealType;
    private String itsMinAmount;
    private String itsMaxAmount;
    private String itsAvgAmount;
    private String itsEndingPrice;

    public InstrumentView(String instrumentid, String instrumentName) {
        itsInstrumentID = instrumentid;
        itsInstrumentName = instrumentName;
    }

    public InstrumentView(String instrumentid, String instrumentName, String dealType, String minAmount, String maxAmount, String avgAmount, String endingPrice) {
        itsInstrumentID = instrumentid;
        itsInstrumentName = instrumentName;
        itsDealType = dealType;
        itsMinAmount = minAmount;
        itsMaxAmount = maxAmount;
        itsAvgAmount = avgAmount;
        itsAvgAmount = avgAmount;
        itsEndingPrice = endingPrice;
    }

    public String getInstrumentID() {
        return itsInstrumentID;
    }

    public void setInstrumentID(String itsInstrumentID) {
        this.itsInstrumentID = itsInstrumentID;
    }

    public String getInstrumentName() {
        return itsInstrumentName;
    }

    public void setInstrumentName(String itsInstrumentName) {
        this.itsInstrumentName = itsInstrumentName;
    }

    public String getDealType() {
        return itsDealType;
    }

    public void setDealType(String itsDealType) {
        this.itsDealType = itsDealType;
    }

    public String getMinAmount() {
        return itsMinAmount;
    }

    public void setMinAmount(String itsMinAmount) {
        this.itsMinAmount = itsMinAmount;
    }

    public String getMaxAmount() {
        return itsMaxAmount;
    }

    public void setMaxAmount(String itsMaxAmount) {
        this.itsMaxAmount = itsMaxAmount;
    }

    public String getAvgAmount() {
        return itsAvgAmount;
    }

    public void setAvgAmount(String itsAvgAmount) {
        this.itsAvgAmount = itsAvgAmount;
    }

    public String getEndingPrice() {
        return itsEndingPrice;
    }

    public void setEndingPrice(String itsEndingPrice) {
        this.itsEndingPrice = itsEndingPrice;
    }
}

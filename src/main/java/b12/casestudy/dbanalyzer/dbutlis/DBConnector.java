package b12.casestudy.dbanalyzer.dbutlis;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author B12
 */
public class DBConnector {

    static private DBConnector itsSelf = null;

    private Connection itsConnection;
    private String dbDriver = "";   //  "com.mysql.jdbc.Driver";
    private String dbPath = "";    //  "jdbc:mysql://127.0.0.1:3307/";
    private String dbName = "";    //  "db_grad";
    private String dbUser = "";    //  "root";
    private String dbPwd = "";     //  "ppp";

    static public DBConnector getConnector() throws IOException {
        if (itsSelf == null) {
            itsSelf = new DBConnector();
        }
        return itsSelf;
    }

    private DBConnector() {
    }

    public Connection getConnection() {
        return itsConnection;
    }

    public boolean connect(Properties properties) {
        boolean result = false;
        try {

            dbDriver = properties.getProperty("dbDriver");
            dbPath = properties.getProperty("dbPath");
            dbName = properties.getProperty("dbName");
            dbUser = properties.getProperty("dbUser");
            dbPwd = properties.getProperty("dbPwd");

            Class.forName(dbDriver);

            itsConnection = DriverManager.getConnection(dbPath + dbName,
                    dbUser,
                    dbPwd);

            result = true;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return false;
        }

        return result;
    }
}

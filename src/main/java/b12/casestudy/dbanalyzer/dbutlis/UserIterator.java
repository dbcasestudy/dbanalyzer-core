package b12.casestudy.dbanalyzer.dbutlis;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author B12
 */
public class UserIterator {

    ResultSet rowIterator = null;

    UserIterator(ResultSet rs) {
        if (rs != null) {
            rowIterator = rs;
        }
    }

    public boolean first() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.first();
        }
        return false;
    }

    public boolean last() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.last();
        }
        return false;
    }

    public boolean next() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.next();
        }
        return false;
    }

    public boolean prior() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.previous();
        }
        return false;
    }

    public String getUserId() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("user_id");
        }
        return null;
    }

    public String getUserPwd() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("user_pwd");
        }
        return null;
    }

    User buildUser() throws SQLException {
        User result = new User(getUserId(), getUserPwd());

        return result;
    }
}

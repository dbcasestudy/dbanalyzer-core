package b12.casestudy.dbanalyzer.dbutlis;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author B12
 */
public class ViewCounterpartyIterator {

    ResultSet rowIterator = null;

    ViewCounterpartyIterator(ResultSet rs) {
        if (rs != null) {
            rowIterator = rs;
        }
    }

    public boolean first() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.first();
        }
        return false;
    }

    public boolean last() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.last();
        }
        return false;
    }

    public boolean next() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.next();
        }
        return false;
    }

    public boolean prior() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.previous();
        }
        return false;
    }

    public String getCounterpartyId() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("counterparty_id");
        }
        return null;
    }

    public String getInstrumentId() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("instrument_id");
        }
        return null;
    }
    
    public String getCounterpartyName() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("counterparty_name");
        }
        return null;
    }
    
    public String getInstrumentName() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("instrument_name");
        }
        return null;
    }
    
    public String getDealQty() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("deal_qty");
        }
        return null;
    }
    
    public String getNetTrades() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("net_trades");
        }
        return null;
    }
    
    public String getEndingPos() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("ending_pos");
        }
        return null;
    }

    public String getEndingSellPrice() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("ending_sell_price");
        }
        return null;
    }

    public String getEndingBuyPrice() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("ending_buy_price");
        }
        return null;
    }

    public String getRealisedProfit() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("realised_profit");
        }
        return null;
    }

    public String getEffectiveProfit() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("effective_profit");
        }
        return null;
    }
        
    CounterpartyView build() throws SQLException {
        CounterpartyView result = new CounterpartyView(getCounterpartyId(), 
                getCounterpartyName(), getInstrumentId(), getInstrumentName(), 
                getDealQty(), getNetTrades(), getEndingPos(), 
                getEndingSellPrice(), getEndingBuyPrice(), getRealisedProfit(), 
                getEffectiveProfit());

        return result;
    }
}

package b12.casestudy.dbanalyzer.dbutlis;

/**
 *
 * @author B12
 */
public class Instrument {

    private String itsInstrumentID;
    private String itsInstrumentName;
    private String itsEndingPrice;

    public Instrument(String instrumentid, String instrumentName, String endingPrice) {
        itsInstrumentID = instrumentid;
        itsInstrumentName = instrumentName;
        itsEndingPrice = endingPrice;
    }

    public String getEndingPrice() {
        return itsEndingPrice;
    }
    
    public void setEndingPrice(String itsEndingPrice) {
        this.itsEndingPrice = itsEndingPrice;
    }
    
    public String getInstrumentID() {
        return itsInstrumentID;
    }

    public void setInstrumentID(String itsInstrumentID) {
        this.itsInstrumentID = itsInstrumentID;
    }

    public String getInstrumentName() {
        return itsInstrumentName;
    }

    public void setInstrumentName(String itsInstrumentName) {
        this.itsInstrumentName = itsInstrumentName;
    }
}

package b12.casestudy.dbanalyzer.dbutlis;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author B12
 */
public class DealIterator {

    ResultSet rowIterator = null;

    DealIterator(ResultSet rs) {
        if (rs != null) {
            rowIterator = rs;
        }
    }

    public boolean first() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.first();
        }
        return false;
    }

    public boolean last() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.last();
        }
        return false;
    }

    public boolean next() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.next();
        }
        return false;
    }

    public boolean prior() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.previous();
        }
        return false;
    }

    public String getDealId() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("deal_id");
        }
        return null;
    }

    public String getDealTime() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("deal_time");
        }
        return null;
    }

    public String getDealType() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("deal_type");
        }
        return null;
    }

    public String getDealAmount() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("deal_amount");
        }
        return null;
    }

    public String getDealQuantity() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("deal_quantity");
        }
        return null;
    }

    public String getDealInstrumentId() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("deal_instrument_id");
        }
        return null;
    }

    public String getCounterpartyId() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("deal_counterparty_id");
        }
        return null;
    }

    // String date, String amount, String time, String type
    Deal build() throws SQLException {
        Deal result = new Deal(getDealId(), getDealTime(),
                getDealType(), getDealAmount(), getDealQuantity(),
                getCounterpartyId(), getDealInstrumentId());
        return result;
    }
} // select deal_id, deal_time, deal_type, deal_amount, deal_quantity, deal_counterparty_id from deal

package b12.casestudy.dbanalyzer.dbutlis;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class ViewInstrumentHandler {

    static private ViewInstrumentHandler itsSelf = null;

    private ViewInstrumentHandler() {
    }

    static public ViewInstrumentHandler getLoader() {
        if (itsSelf == null) {
            itsSelf = new ViewInstrumentHandler();
        }
        return itsSelf;
    }

//
//    private String readFileAsString() throws FileNotFoundException, IOException
//    {
//        ClassLoader classLoader = new InstrumentHandler().getClass().getClassLoader();
//        File file = new File(classLoader.getResource("createViewInstrument.sql").getFile());
//
//        //File is found
//        System.out.println("File Found : " + file.exists());
//        //Read File Content
//        StringBuilder fileData = new StringBuilder();
//        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
//            char[] buf = new char[1024];
//            int numRead = 0;
//            while ((numRead = reader.read(buf)) != -1) {
//                String readData = String.valueOf(buf, 0, numRead);
//                fileData.append(readData);
//            }
//        }
//        return fileData.toString();
//    }
    public InstrumentView[] loadFromDB(Connection theConnection, String baseQuery, String column,
            String filter, String sort, String sortOrder, String limit, String offset) {
        UtilMethods utilMethods = new UtilMethods();
        InstrumentView[] result = null;
        try {
            ArrayList<InstrumentView> tmpResult = new ArrayList<>();
            String sbQuery = utilMethods.createQueryString(baseQuery, column, filter, sort,
                    sortOrder, limit, offset);

            ResultSet rs = utilMethods.getQueryResult(theConnection, sbQuery);

            ViewInstrumentIterator iter = new ViewInstrumentIterator(rs);

            while (iter.next()) {
                tmpResult.add(iter.build());
            }
            result = new InstrumentView[tmpResult.size()];
            result = tmpResult.toArray(result);
        } catch (SQLException ex) {
            Logger.getLogger(ViewInstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public String toJSON(InstrumentView theInstrumentView) {
        String result = "<Some value from the server>";
        try {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(theInstrumentView);
        } catch (IOException ex) {
            Logger.getLogger(ViewInstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

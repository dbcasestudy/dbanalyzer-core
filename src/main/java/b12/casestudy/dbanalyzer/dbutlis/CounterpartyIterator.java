package b12.casestudy.dbanalyzer.dbutlis;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author B12
 */
public class CounterpartyIterator {

    ResultSet rowIterator = null;

    CounterpartyIterator(ResultSet rs) {
        if (rs != null) {
            rowIterator = rs;
        }
    }

    public boolean first() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.first();
        }
        return false;
    }

    public boolean last() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.last();
        }
        return false;
    }

    public boolean next() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.next();
        }
        return false;
    }

    public boolean prior() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.previous();
        }
        return false;
    }

    public String getCounterpartyId() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("counterparty_id");
        }
        return null;
    }

    public String getCounterpartyName() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("counterparty_name");
        }
        return null;
    }

    public String getCounterpartyStatus() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("counterparty_status");
        }
        return null;
    }

    public String getCounterpartyDateRegistered() throws SQLException {
        if (rowIterator != null) {
            return rowIterator.getString("counterparty_date_registered");
        }
        return null;
    }

    public String getEffectiveProfit() throws SQLException {
        return rowIterator.getString("effective_profit");
    }

    public String getRealisedProfit() throws SQLException {
        return rowIterator.getString("realised_profit");
    }

    Counterparty build() throws SQLException {
        Counterparty result = new Counterparty(
                getCounterpartyId(),
                getCounterpartyName(),
                getCounterpartyStatus(),
                getCounterpartyDateRegistered(),
                getRealisedProfit(),
                getEffectiveProfit());

        return result;
    }
}

package b12.casestudy.dbanalyzer.dbutlis;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class InstrumentHandler {

    static private InstrumentHandler itsSelf = null;
    private InstrumentHandler() {
    }

    static public InstrumentHandler getLoader() {
        if (itsSelf == null) {
            itsSelf = new InstrumentHandler();
        }
        return itsSelf;
    }


    public Instrument[] loadFromDB(Connection theConnection, String baseQuery, String column,
            String filter, String sort, String sortOrder, String limit, String offset) {
        Instrument[] result = null;
        try {
            ArrayList<Instrument> tmpResult = new ArrayList<>();

            UtilMethods utilMethods = new UtilMethods();
            
            String sbQuery = utilMethods.createQueryString(baseQuery, column, filter, sort, 
                    sortOrder, limit, offset);
            
            ResultSet rs = utilMethods.getQueryResult(theConnection, sbQuery);
            
            InstrumentIterator iter = new InstrumentIterator(rs);

            while (iter.next()) {
                tmpResult.add(iter.buildInstrument());
            }
            result = new Instrument[tmpResult.size()];
            result = tmpResult.toArray(result);
        } catch (SQLException ex) {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    public String toJSON(Instrument theInstrument) {
        String result = "<Some value from the server>";
        try {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(theInstrument);
        } catch (IOException ex) {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}

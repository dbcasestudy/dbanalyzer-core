package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.User;

/**
 *
 * @author B12
 */
public class UserController {

    final private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public String verifyLoginDetails(String userId, String userPwd) {
        String result = null;

        User theUser = ash.userLogin(userId, userPwd);

        if (theUser != null) {
            result = theUser.getUserID();
        }

        return result;
    }
}

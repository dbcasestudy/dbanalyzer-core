package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.DBConnector;
import b12.casestudy.dbanalyzer.dbutlis.Instrument;
import b12.casestudy.dbanalyzer.dbutlis.InstrumentHandler;
import b12.casestudy.dbanalyzer.dbutlis.InstrumentView;
import b12.casestudy.dbanalyzer.dbutlis.UtilMethods;
import b12.casestudy.dbanalyzer.dbutlis.ViewInstrumentHandler;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class ViewInstrumentController {

    final private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public String getTable(String column, String filter, String sort, String sortOrder,
            String limit, String offset) {
        StringBuilder result = new StringBuilder("");
        UtilMethods utilMethods = new UtilMethods();
        ResultSet instrumentViewsCounter = null;
        try {
            String baseCountQuery = "select count(*) from b12_view_instrument";
            if (column != null && filter != null) {
                baseCountQuery += " where " + utilMethods.getFilterExpression(column, filter);
            }
            instrumentViewsCounter = utilMethods.getQueryResult(DBConnector.getConnector().
                    getConnection(), baseCountQuery);
        } catch (IOException ex) {
            Logger.getLogger(ViewDealController.class.getName()).log(Level.SEVERE, null, ex);
        }
        InstrumentView[] instrumentsView = ash.getInstrumentViewTable(column, filter, sort, 
                sortOrder, limit, offset);
        if (instrumentsView != null) {
            result = new StringBuilder("[");
            try {
                instrumentViewsCounter.next();
                result.append("{\"size\":\"" + String.valueOf(instrumentViewsCounter.getString(1)) + "\"},\n");
            } catch (SQLException ex) {
                Logger.getLogger(ViewInstrumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (InstrumentView currentInstrumentView : instrumentsView) {
                if (currentInstrumentView != null) {
                    result.append(ViewInstrumentHandler.getLoader().toJSON(currentInstrumentView));
                    result.append(",\n");
                }
            }
        
            result.delete(result.length() - 2, result.length());

            result.append("]");
        }
        return result.toString();
    }
}

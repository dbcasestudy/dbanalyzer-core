package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.Counterparty;
import b12.casestudy.dbanalyzer.dbutlis.CounterpartyHandler;
import b12.casestudy.dbanalyzer.dbutlis.CounterpartyView;
import b12.casestudy.dbanalyzer.dbutlis.DBConnector;
import b12.casestudy.dbanalyzer.dbutlis.Deal;
import b12.casestudy.dbanalyzer.dbutlis.DealHandler;
import b12.casestudy.dbanalyzer.dbutlis.DealView;
import b12.casestudy.dbanalyzer.dbutlis.Instrument;
import b12.casestudy.dbanalyzer.dbutlis.InstrumentHandler;
import b12.casestudy.dbanalyzer.dbutlis.InstrumentView;
import b12.casestudy.dbanalyzer.dbutlis.User;
import b12.casestudy.dbanalyzer.dbutlis.UserHandler;
import b12.casestudy.dbanalyzer.dbutlis.UtilMethods;
import b12.casestudy.dbanalyzer.dbutlis.ViewCounterpartyHandler;
import b12.casestudy.dbanalyzer.dbutlis.ViewDealHandler;
import b12.casestudy.dbanalyzer.dbutlis.ViewInstrumentHandler;
import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.google.common.io.Resources;
import java.io.IOException;
import java.net.URL;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class ApplicationScopeHelper {

    private DBConnector itsConnector = null;

    private String itsInfo = "NOT SET";

    public String getInfo() {
        return itsInfo;
    }

    public void setInfo(String itsInfo) {
        this.itsInfo = itsInfo;
    }

    public User userLogin(String userId, String userPwd) {
        User theUser = null;
        try {
            UserHandler theUserHandler = UserHandler.getLoader();

            theUser = theUserHandler.loadFromDB(DBConnector.getConnector().getConnection(), userId, userPwd);

        } catch (IOException ex) {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return theUser;
    }

    public Deal[] getDealTable(String column, String filter, String sort,
            String sortOrder, String limit, String offset) {
        Deal[] deals = null;
        String baseQuery = "select * from deal";
        try {
            DealHandler theDealHandler = DealHandler.getLoader();
            deals = theDealHandler.loadFromDB(DBConnector.getConnector().
                    getConnection(), baseQuery, column, filter, sort, sortOrder, limit, offset);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return deals;
    }

    public DealView[] getDealViewTable(String column, String filter, String sort,
            String sortOrder, String limit, String offset) {
        DealView[] dealViews = null;

        String baseQuery = "select * from b12_view_deal";
        try {
            ViewDealHandler theViewDealHandler = ViewDealHandler.getLoader();
            dealViews = theViewDealHandler.loadFromDB(DBConnector.getConnector().
                    getConnection(), baseQuery, column, filter, sort, sortOrder, limit, offset);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dealViews;
    }

    public Instrument[] getInstrumentTable(String column, String filter,
            String sort, String sortOrder, String limit, String offset) {
        Instrument[] instruments = null;
        String baseQuery = "select * from b12_view_instrument_with_price";
        try {
            InstrumentHandler theInstrumentHandler = InstrumentHandler.getLoader();
            instruments = theInstrumentHandler.loadFromDB(DBConnector.getConnector().
                    getConnection(), baseQuery, column, filter, sort, sortOrder, limit, offset);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return instruments;
    }

    public InstrumentView[] getInstrumentViewTable(String column, String filter,
            String sort, String sortOrder, String limit, String offset) {
        InstrumentView[] instrumentsView = null;
        String baseQuery = "select * from b12_view_instrument";
        try {
            ViewInstrumentHandler theViewInstrumentHandler = ViewInstrumentHandler.getLoader();
            instrumentsView = theViewInstrumentHandler.loadFromDB(DBConnector.getConnector().
                    getConnection(), baseQuery, column, filter, sort, sortOrder, limit, offset);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return instrumentsView;
    }

    public CounterpartyView[] getCounterpartyViewTable(String column, String filter,
            String sort, String sortOrder, String limit, String offset) {
        CounterpartyView[] counterpartyViews = null;
        String baseQuery = "select * from b12_view_counterparty_detail";
        try {
            ViewCounterpartyHandler theViewCounterpartyHandler = ViewCounterpartyHandler.getLoader();
            counterpartyViews = theViewCounterpartyHandler.loadFromDB(DBConnector.getConnector().
                    getConnection(), baseQuery, column, filter, sort, sortOrder, limit, offset);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return counterpartyViews;

    }

    public Counterparty[] getCounterpartyTable(String column, String filter,
            String sort, String sortOrder, String limit, String offset) {
        Counterparty[] counterparties = null;
        String baseQuery = "select * from b12_view_counterparty_with_profit";
        try {
            CounterpartyHandler theCounterpartyHandler = CounterpartyHandler.getLoader();
            counterparties = theCounterpartyHandler.loadFromDB(DBConnector.getConnector().
                    getConnection(), baseQuery, column, filter, sort, sortOrder, limit, offset);
        } catch (IOException ex) {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return counterparties;
    }

    public boolean bootstrapDBConnection() {
        boolean result = false;
        if (itsConnector == null) {
            try {
                itsConnector = DBConnector.getConnector();

                PropertyLoader pLoader = PropertyLoader.getLoader();

                Properties pp;
                pp = pLoader.getPropValues("dbConnector.properties");

                result = itsConnector.connect(pp);
                if (!result) {
                    return false;
                }
                createTempCredentialsTable();

                if (!viewsExisted()) {
                    createViews();
                }
            } catch (IOException | SQLException ex) {
                Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } else {
            result = true;
        }

        return result;
    }

    private boolean viewsExisted() throws SQLException {
        String[] views = {"b12_view_instrument", "b12_view_deal", "b12_view_counterparty_detail"};
        DatabaseMetaData dbm = itsConnector.getConnection().getMetaData();
        for (String currentViewName : views) {
            if (!dbm.getTables(null, null, currentViewName, null).next()) {
                return false;
            }
        }
        return true;
    }

    private void createViews() throws SQLException, IOException {
        UtilMethods utilMethods = new UtilMethods();
        utilMethods.execute(itsConnector.getConnection(), getSqlForViews());
    }

    private void createTempCredentialsTable() throws SQLException, IOException {
        UtilMethods utilMethods = new UtilMethods();
        utilMethods.execute(itsConnector.getConnection(), "CREATE TEMPORARY TABLE creds (user_id varchar(40), user_pwd varchar(256));");

        ResultSet users = utilMethods.getQueryResult(itsConnector.getConnection(), "select * from users");

        while (users.next()) {
            String id = users.getString(1);
            String pwd = users.getString(2);
            String encryptedPwd = Hashing.sha256().
                    hashString(pwd, Charsets.UTF_8).toString();
            utilMethods.execute(itsConnector.getConnection(), "INSERT INTO creds VALUES"
                    + "("
                    + "\'" + id + "\'"
                    + ","
                    + "\'" + encryptedPwd + "\'"
                    + ")"
            );
        }

//        utilMethods.execute(itsConnector.getConnection(), "");
    }

    private String getSqlForViews() throws IOException {
        URL url = Resources.getResource("views.sql");
        return Resources.toString(url, Charsets.UTF_8);
    }
}

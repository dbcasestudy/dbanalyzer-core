package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.DBConnector;
import b12.casestudy.dbanalyzer.dbutlis.Instrument;
import b12.casestudy.dbanalyzer.dbutlis.InstrumentHandler;
import b12.casestudy.dbanalyzer.dbutlis.UtilMethods;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class InstrumentController {

    final private ApplicationScopeHelper ash = new ApplicationScopeHelper();

//    public String getFakeTable(String column, String filter, String sort, String sortOrder,
//            String limit, String offset) {
//        String fakeResult = null;
//        try {
//            fakeResult = getTable(column, filter, sort, sortOrder, limit, offset);
//        } catch (Exception e) {
//            return null;
//        }
//        if (fakeResult != null) {
//            return "success";
//        }
//        return "fail";
//    }
    
    public String getTable(String column, String filter, String sort, String sortOrder,
            String limit, String offset) {
        StringBuilder result = new StringBuilder("");
        UtilMethods utilMethods = new UtilMethods();
        ResultSet instrumentCounter = null;
        try {
            String baseCountQuery = "select count(*) from b12_view_instrument_with_price";
            if (column != null && filter != null) {
                baseCountQuery += " where " + utilMethods.getFilterExpression(column, filter);
            }
            instrumentCounter = utilMethods.getQueryResult(DBConnector.getConnector().
                    getConnection(), baseCountQuery);
        } catch (IOException ex) {
            Logger.getLogger(ViewDealController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Instrument[] instruments = ash.getInstrumentTable(column, filter, sort, 
                sortOrder, limit, offset);
        if (instruments != null) {
            result = new StringBuilder("[");
            try {
                instrumentCounter.next();
                result.append("{\"size\":\"" + String.valueOf(instrumentCounter.getString(1)) + "\"},\n");
            } catch (SQLException ex) {
                Logger.getLogger(InstrumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (Instrument currentInstrument : instruments) {
                if (currentInstrument != null) {
                    result.append(InstrumentHandler.getLoader().toJSON(currentInstrument));
                    result.append(",\n");
                }
            }
        
            result.delete(result.length() - 2, result.length());

            result.append("]");
        }
        return result.toString();
    }
}

package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.Counterparty;
import b12.casestudy.dbanalyzer.dbutlis.DBConnector;
import b12.casestudy.dbanalyzer.dbutlis.Deal;
import b12.casestudy.dbanalyzer.dbutlis.DealHandler;
import b12.casestudy.dbanalyzer.dbutlis.UtilMethods;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class DealController {

    final private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public String getTable(String column, String filter, String sort, 
            String sortOrder, String limit, String offset) {
        StringBuilder result = new StringBuilder("");
        UtilMethods utilMethods = new UtilMethods();
        ResultSet dealCounter = null;
        try {
            String baseCountQuery = "select count(*) from deal";
            if (column != null && filter != null) {
                baseCountQuery += " where " + utilMethods.getFilterExpression(column, filter);
            }
            dealCounter = utilMethods.getQueryResult(DBConnector.getConnector().
                    getConnection(), baseCountQuery);
        } catch (IOException ex) {
            Logger.getLogger(ViewDealController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Deal[] deals = ash.getDealTable(column, filter, sort, sortOrder, limit, offset);
        if (deals != null) {
            result = new StringBuilder("[");
            try {
                dealCounter.next();
                result.append("{\"size\":\"" + dealCounter.getString(1) + "\"},\n");
            } catch (SQLException ex) {
                Logger.getLogger(DealController.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (Deal currentDeal : deals) {
                if (currentDeal != null) {
                    result.append(DealHandler.getLoader().toJSON(currentDeal));
                    result.append(",\n");
                }
            }
        
            result.delete(result.length() - 2, result.length());

            result.append("]");
        }
        return result.toString();
    }
}

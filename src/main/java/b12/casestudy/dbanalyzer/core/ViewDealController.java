package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.DBConnector;
import b12.casestudy.dbanalyzer.dbutlis.Deal;
import b12.casestudy.dbanalyzer.dbutlis.DealHandler;
import b12.casestudy.dbanalyzer.dbutlis.DealView;
import b12.casestudy.dbanalyzer.dbutlis.UtilMethods;
import b12.casestudy.dbanalyzer.dbutlis.ViewDealHandler;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class ViewDealController {

    final private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public String getTable(String column, String filter, String sort, 
            String sortOrder, String limit, String offset) {
        StringBuilder result = new StringBuilder("");
        UtilMethods utilMethods = new UtilMethods();
        ResultSet dealViewsCounter = null;
        try {
            String baseCountQuery = "select count(*) from b12_view_deal";
            if (column != null && filter != null) {
                baseCountQuery += " where " + utilMethods.getFilterExpression(column, filter);
            }
            dealViewsCounter = utilMethods.getQueryResult(DBConnector.getConnector().
                    getConnection(), baseCountQuery);
        } catch (IOException ex) {
            Logger.getLogger(ViewDealController.class.getName()).log(Level.SEVERE, null, ex);
        }
        DealView[] dealViews = ash.getDealViewTable(column, filter, sort, sortOrder, limit, offset);
        if (dealViews != null) {
            result = new StringBuilder("[");
            try {
                dealViewsCounter.next();
                result.append("{\"size\":\"" + String.valueOf(dealViewsCounter.getString(1)) + "\"},\n");
            } catch (SQLException ex) {
                Logger.getLogger(ViewDealController.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (DealView currentDealView : dealViews) {
                if (currentDealView != null) {
                    result.append(ViewDealHandler.getLoader().toJSON(currentDealView));
                    result.append(",\n");
                }
            }
        
            result.delete(result.length() - 2, result.length());

            result.append("]");
        }
        return result.toString();
    }
}

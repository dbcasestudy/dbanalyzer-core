package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.Counterparty;
import b12.casestudy.dbanalyzer.dbutlis.CounterpartyHandler;
import b12.casestudy.dbanalyzer.dbutlis.DBConnector;
import b12.casestudy.dbanalyzer.dbutlis.UtilMethods;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class CounterpartyController {

    final private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public String getTable(String column, String filter, String sort,
            String sortOrder, String limit, String offset) {
        StringBuilder result = new StringBuilder("");
        UtilMethods utilMethods = new UtilMethods();
        ResultSet counterpartyCounter = null;
        try {
            String baseCountQuery = "select count(*) from b12_view_counterparty_with_profit";
            if (column != null && filter != null) {
                baseCountQuery += " where " + utilMethods.getFilterExpression(column, filter);
            }
            counterpartyCounter = utilMethods.getQueryResult(DBConnector.getConnector().
                    getConnection(), baseCountQuery);
        } catch (IOException ex) {
            Logger.getLogger(ViewDealController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Counterparty[] counterparties = ash.getCounterpartyTable(column, filter,
                sort, sortOrder, limit, offset);
        if (counterparties != null) {
            result = new StringBuilder("[");
            try {
                counterpartyCounter.next();
                result.append("{\"size\":\"" + String.valueOf(counterpartyCounter.getString(1)) + "\"},\n");
            } catch (SQLException ex) {
                Logger.getLogger(CounterpartyController.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (Counterparty currentCounterparty : counterparties) {
                if (currentCounterparty != null) {
                    result.append(CounterpartyHandler.getLoader().toJSON(currentCounterparty));
                    result.append(",\n");
                }
            }

            result.delete(result.length() - 2, result.length());

            result.append("]");
        }
        return result.toString();
    }
}

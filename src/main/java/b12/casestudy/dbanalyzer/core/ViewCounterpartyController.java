package b12.casestudy.dbanalyzer.core;

import b12.casestudy.dbanalyzer.dbutlis.Counterparty;
import b12.casestudy.dbanalyzer.dbutlis.CounterpartyHandler;
import b12.casestudy.dbanalyzer.dbutlis.CounterpartyView;
import b12.casestudy.dbanalyzer.dbutlis.DBConnector;
import b12.casestudy.dbanalyzer.dbutlis.UtilMethods;
import b12.casestudy.dbanalyzer.dbutlis.ViewCounterpartyHandler;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author B12
 */
public class ViewCounterpartyController {

    final private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public String getTable(String column, String filter, String sort, 
            String sortOrder, String limit, String offset) {
        StringBuilder result = new StringBuilder("");
        UtilMethods utilMethods = new UtilMethods();
        ResultSet counterpartyViewsCounter = null;
        try {
            String baseCountQuery = "select count(*) from b12_view_counterparty_detail";
            if (column != null && filter != null) {
                baseCountQuery += " where " + utilMethods.getFilterExpression(column, filter);
            }
            counterpartyViewsCounter = utilMethods.getQueryResult(DBConnector.getConnector().
                    getConnection(), baseCountQuery);
        } catch (IOException ex) {
            Logger.getLogger(ViewDealController.class.getName()).log(Level.SEVERE, null, ex);
        }
        CounterpartyView[] counterpartyViews = ash.getCounterpartyViewTable(column, filter, 
                sort, sortOrder, limit, offset);
        if (counterpartyViews != null) {
            result = new StringBuilder("[");
            try {
                counterpartyViewsCounter.next();
                result.append("{\"size\":\"" + String.valueOf(counterpartyViewsCounter.getString(1)) + "\"},\n");
            } catch (SQLException ex) {
                Logger.getLogger(ViewCounterpartyController.class.getName()).log(Level.SEVERE, null, ex);
            }
            for (CounterpartyView currentCounterparty : counterpartyViews) {
                if (currentCounterparty != null) {
                    result.append(ViewCounterpartyHandler.getLoader().toJSON(currentCounterparty));
                    result.append(",\n");
                }
            }
        
            result.delete(result.length() - 2, result.length());

            result.append("]");
        }
        return result.toString();
    }
}

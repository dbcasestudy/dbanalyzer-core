CREATE VIEW b12_view_instrument_with_price AS

SELECT
instrument_id,
instrument_name,
(SELECT deal_amount FROM deal d WHERE i.instrument_id = d.deal_instrument_id ORDER BY deal_time DESC LIMIT 1) AS ending_price
FROM instrument i;

CREATE VIEW b12_view_instrument AS
 
SELECT instrument_id, 
instrument_name, 
deal_type, 
min(deal_amount) as min_amount, 
max(deal_amount) as max_amount, 
avg(deal_amount) as avg_amount,
(SELECT deal_amount FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_type = d.deal_type ORDER BY deal_time DESC LIMIT 1) AS ending_price
FROM instrument i, deal d 
WHERE d.deal_instrument_id=i.instrument_id 
GROUP BY instrument_id, deal_type;

CREATE VIEW b12_view_deal AS

SELECT
  deal_id,
  deal_time,
  deal_type,
  deal_amount,
  deal_quantity,
  deal_amount*deal_quantity as deal_cost,
  deal_instrument_id,
  instrument_name as deal_instrument_name,
  deal_counterparty_id,
  counterparty_name as deal_counterparty_name
FROM instrument i, deal d, counterparty c
WHERE d.deal_instrument_id=i.instrument_id AND d.deal_counterparty_id=c.counterparty_id;

CREATE VIEW b12_view_counterparty_detail_h AS
SELECT
  deal_counterparty_id as counterparty_id,
  (SELECT counterparty_name FROM counterparty WHERE deal_counterparty_id=counterparty_id) as counterparty_name,
  deal_instrument_id as instrument_id,
  (SELECT instrument_name FROM instrument WHERE deal_instrument_id=instrument_id) instrument_name,
  count(deal_id) as deal_qty,
  (SELECT SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'S') AS net_trades,
  (SELECT SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'B') -
  (SELECT SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'S') AS ending_pos,
(SELECT deal_amount FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND deal_type = 'S' ORDER BY deal_time DESC LIMIT 1) AS ending_sell_price,
(SELECT deal_amount FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND deal_type = 'B' ORDER BY deal_time DESC LIMIT 1) AS ending_buy_price,
(SELECT SUM(deal_amount*deal_quantity)/SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'S') AS avg_sell_price,
(SELECT SUM(deal_amount*deal_quantity)/SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'B') AS avg_buy_price,
  ((SELECT SUM(deal_amount*deal_quantity)/SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'S') -
  (SELECT SUM(deal_amount*deal_quantity)/SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'B')) *
  (if(
(SELECT SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'S') <
(SELECT SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'B'),
(SELECT SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'S'),
(SELECT SUM(deal_quantity) FROM deal d2 WHERE d2.deal_instrument_id = d.deal_instrument_id AND d2.deal_counterparty_id = d.deal_counterparty_id AND deal_type = 'B'))) AS realised_profit
FROM deal d
group by deal_counterparty_id, deal_instrument_id;

CREATE VIEW b12_view_counterparty_detail AS
SELECT
  *,
  (realised_profit + ending_pos*(if(ending_pos > 0, ending_sell_price, ending_buy_price)) -
 (if(ending_pos > 0, avg_buy_price, avg_sell_price))) as effective_profit
FROM b12_view_counterparty_detail_h;

CREATE OR REPLACE VIEW b12_view_counterparty_with_profit AS
SELECT
counterparty_id,
counterparty_name,
counterparty_status,
counterparty_date_registered,
(SELECT SUM(realised_profit) from b12_view_counterparty_detail vc where vc.counterparty_id=c.counterparty_id) AS realised_profit,
(SELECT SUM(effective_profit) from b12_view_counterparty_detail vc where vc.counterparty_id=c.counterparty_id) AS effective_profit
FROM counterparty c;
